package com.itzhp.mobi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.itzhp.mobi.R;
import com.itzhp.mobi.db.DatabaseHelper;
import com.itzhp.mobi.room.Rooms;
import java.util.ArrayList;

public class Roomadapter extends RecyclerView.Adapter<Roomadapter.Imagevh> {
    ArrayList<String> name=new ArrayList<>();
    public Context context;
    public Roomadapter(ArrayList<String> product_name){
        this.name=product_name;
           }

    @NonNull
    @Override
    public Imagevh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.roomslist,parent,false);
        Imagevh imagevh=new Imagevh(view);

        context=parent.getContext();
        return imagevh;
    }

    @Override
    public void onBindViewHolder(@NonNull final Imagevh holder, final int position) {
        holder.name.setText("Room "+name.get(position));
        holder.bookbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Rooms) context).Book(holder.getAdapterPosition());
            }
        });

    }

    @Override
    public int getItemCount() {
        return name.size();
    }

    public  static class Imagevh extends RecyclerView.ViewHolder{

        Button bookbutton;
        TextView name;
        public Imagevh(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            bookbutton=itemView.findViewById(R.id.book);

        }
    }
}

