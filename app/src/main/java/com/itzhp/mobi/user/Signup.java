package com.itzhp.mobi.user;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.itzhp.mobi.R;
import com.itzhp.mobi.db.DatabaseHelper;

public class Signup extends AppCompatActivity {
    TextInputEditText fname,lname,phno,pass;
    DatabaseHelper myDb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        fname=findViewById(R.id.firstname);
        lname=findViewById(R.id.lastnameid);
        phno=findViewById(R.id.phno);
        pass=findViewById(R.id.password);
        myDb = new DatabaseHelper(this);
    }

    public void Signup(View view) {
        if( phno.getText().toString().isEmpty() || pass.getText().toString().isEmpty()|| fname.getText().toString().isEmpty() ){
            phno.setError( "Required" );
            pass.setError( "Required" );
            fname.setError( "Required" );
        }else {
            boolean check=myDb.insertuserData(fname.getText().toString() +" "+ lname.getText().toString(),pass.getText().toString() ,phno.getText().toString());
            if (check){
                Toast.makeText(this, "Successfully Created Account", Toast.LENGTH_LONG).show();
                startActivity(new Intent(Signup.this,Login.class));
            }
        }
    }

    public void signin(View view) {
        startActivity(new Intent(Signup.this,Login.class));
    }
}