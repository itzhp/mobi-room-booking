package com.itzhp.mobi.user;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.itzhp.mobi.Config;
import com.itzhp.mobi.R;
import com.itzhp.mobi.db.DatabaseHelper;
import com.itzhp.mobi.room.HomePage;

public class Login extends AppCompatActivity {
    TextInputEditText phno,pass;
    DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        phno=(TextInputEditText) findViewById(R.id.pno);
        pass=(TextInputEditText)findViewById(R.id.pass);
        myDb = new DatabaseHelper(this);
        SharedPreferences sharedPreferences = getSharedPreferences(Config.LOGGEDIN_SHARED_PREF, Context.MODE_PRIVATE);
        String loggedIn = sharedPreferences.getString(Config.LOGIN_SUCCESS, "false");
        if(loggedIn.equals("true")){
            Intent intent = new Intent(Login.this, HomePage.class);
            startActivity(intent);
        }
    }

    public void signin(View view) {
        final String phone = phno.getText().toString().trim();
        final String password = pass.getText().toString().trim();
        if(!phone.isEmpty()|| !password.isEmpty()){
            Cursor cursor = myDb.login(phone,password);
            String id="0";
            if (cursor.getCount() != 0) {
                while (cursor.moveToNext()) {
                     id = cursor.getString(0);
                }
                SharedPreferences sharedPreferences = Login.this.getSharedPreferences(Config.SHARED_ID, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Config.CUS_ID,id);
                editor.apply();

                SharedPreferences spln = getSharedPreferences(Config.LOGGEDIN_SHARED_PREF,Context.MODE_PRIVATE);
                SharedPreferences.Editor edln = spln.edit();
                edln.putString(Config.LOGIN_SUCCESS, "true");
                edln.apply();

                startActivity(new Intent(Login.this, HomePage.class));

            } else {
                Toast.makeText(Login.this, "You are not a valid user", Toast.LENGTH_LONG).show();
            }
            myDb.close();
        }else {
            phno.setError("Required");
            pass.setError("Required");
        }
    }

    public void signup(View view) {
        startActivity(new Intent(Login.this,Signup.class));
    }
}