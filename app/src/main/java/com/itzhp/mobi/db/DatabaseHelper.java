package com.itzhp.mobi.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * Created by ProgrammingKnowledge on 4/3/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "mobibook.db";
    public static final String TABLE_NAME = "booking";
    public static final String USER_TABLE_NAME = "user";
    public static final String COL_1 = "id";
    public static final String COL_2 = "room_no";
    public static final String COL_3 = "from_date";
    public static final String COL_4 = "to_date";
    public static final String COL_5 = "force_checkout";
    public static final String COL_6 = "userid";

    public static final String COL_7 = "name";
    public static final String COL_8 = "password";
    public static final String COL_9 = "phone";
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + TABLE_NAME + "(id INTEGER PRIMARY KEY AUTOINCREMENT,room_no TEXT,from_date TEXT,to_date TEXT,force_checkout TEXT,userid TEXT)");

        db.execSQL("create table " + USER_TABLE_NAME + "(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,password TEXT,phone TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE_NAME);
        onCreate(db);
    }
    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public boolean insertuserData(String name,String password,String phone) {
        long result = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_7, name);
        contentValues.put(COL_8, md5(password));
        contentValues.put(COL_9, phone);
        result = db.insert(USER_TABLE_NAME, null, contentValues);
        return result != -1;
    }

    public Cursor login(String phone,String password) {
        SQLiteDatabase dbh = this.getWritableDatabase();
        Cursor myCursor = dbh.rawQuery("SELECT * FROM "+USER_TABLE_NAME+" WHERE "+COL_9+"= "+phone+" AND "+COL_8+"= '"+md5(password)+"'", null);
        return myCursor;
    }
    public boolean insertData(String roomno,String fromdate,String todate,String fcheckout,String uid) {
                long result = 0;
                SQLiteDatabase db = this.getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put(COL_2, roomno);
                contentValues.put(COL_3, fromdate);
                contentValues.put(COL_4, todate);
                contentValues.put(COL_5, fcheckout);
                contentValues.put(COL_6, uid);
                result = db.insert(TABLE_NAME, null, contentValues);
        return result != -1;
    }

    public Cursor getRoom(String sdate,String edate, String roomno) {
        SQLiteDatabase dbh = this.getWritableDatabase();
        Cursor myCursor = dbh.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE "+COL_2+"= "+roomno+" AND (date("+COL_3+")>Datetime('"+edate+"') OR date("+COL_4+")>Datetime('"+sdate+"')) AND "+COL_5+"='1'", null);
        return myCursor;
    }
    public Cursor getBookrooms(String uid) {
        SQLiteDatabase dbh = this.getWritableDatabase();
        Cursor myCursor = dbh.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE "+COL_6+"= "+uid+" AND "+COL_5+"='1'", null);
        return myCursor;
    }
    public void update(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("UPDATE "+TABLE_NAME+" SET "+COL_5+ "= '0' WHERE "+COL_1 +"  ="+id);
    }
   }
