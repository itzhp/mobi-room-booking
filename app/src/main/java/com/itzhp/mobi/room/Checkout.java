package com.itzhp.mobi.room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.Toast;

import com.itzhp.mobi.Config;
import com.itzhp.mobi.R;
import com.itzhp.mobi.adapter.Checkoutadapter;
import com.itzhp.mobi.adapter.Roomadapter;
import com.itzhp.mobi.db.DatabaseHelper;

import java.util.ArrayList;

public class Checkout extends AppCompatActivity {
    ArrayList<String> room=new ArrayList<>();
    ArrayList<String> id=new ArrayList<>();
    DatabaseHelper myDb;
    private RecyclerView recyclerView;
    private Checkoutadapter checkoutadapter;
    private  RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        recyclerView=findViewById(R.id.rec);
        myDb = new DatabaseHelper(this);
        SharedPreferences spt=Checkout.this.getSharedPreferences(Config.SHARED_ID, Context.MODE_PRIVATE);

        Cursor cursor = myDb.getBookrooms(spt.getString(Config.CUS_ID,"0"));

        if (cursor.getCount() != 0) {
            while (cursor.moveToNext()) {
                id.add(cursor.getString(0));
                room.add(cursor.getString(1));
            }
        }

        layoutManager=new LinearLayoutManager(this);
        try {
            recyclerView.setHasFixedSize(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        recyclerView.setLayoutManager(layoutManager);
        checkoutadapter=new Checkoutadapter(room);
        recyclerView.setAdapter(checkoutadapter);
    }

    public void Checkout(int adapterPosition) {
        myDb.update(Integer.parseInt(id.get(adapterPosition)));
        id.remove(adapterPosition);
        room.remove(adapterPosition);
        checkoutadapter.notifyDataSetChanged();
        Toast.makeText(this,"You Room Has Been Checkout", Toast.LENGTH_LONG).show();

    }
}