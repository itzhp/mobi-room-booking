package com.itzhp.mobi.room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.Toast;

import com.itzhp.mobi.Config;
import com.itzhp.mobi.R;
import com.itzhp.mobi.adapter.Roomadapter;
import com.itzhp.mobi.db.DatabaseHelper;

import java.util.ArrayList;

public class Rooms extends AppCompatActivity {
    ArrayList<String> room=new ArrayList<>();
    DatabaseHelper myDb;
    private RecyclerView recyclerView;
    private Roomadapter roomadapter;
    private  RecyclerView.LayoutManager layoutManager;
    String fdata,todata;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);
        recyclerView=findViewById(R.id.rec);
        myDb = new DatabaseHelper(this);
         fdata = getIntent().getExtras().getString("fdate","0");
         todata = getIntent().getExtras().getString("todate","0");
        room.clear();
        for (int i=1;i<=10;i++){
            Cursor cursor = myDb.getRoom(fdata+" 00:00:00",todata+" 00:00:00",String.valueOf(i));
             if (cursor.getCount() == 0) {
                room.add(String.valueOf(i));
            }
            myDb.close();
        }
        layoutManager=new LinearLayoutManager(this);
        try {
            recyclerView.setHasFixedSize(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        recyclerView.setLayoutManager(layoutManager);
        roomadapter=new Roomadapter(room);
        recyclerView.setAdapter(roomadapter);
    }

    public void Book(int position) {
        SharedPreferences spt=Rooms.this.getSharedPreferences(Config.SHARED_ID, Context.MODE_PRIVATE);
        boolean tst=myDb.insertData(room.get(position), fdata+" 00:00:00", todata+" 00:00:00", "1", spt.getString(Config.CUS_ID,"0"));
        if(tst){
            room.remove(position);
            roomadapter.notifyDataSetChanged();
            Toast.makeText(this, "Room Booked", Toast.LENGTH_LONG).show();
        }
    }
}