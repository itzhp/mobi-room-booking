package com.itzhp.mobi.room;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.itzhp.mobi.Config;
import com.itzhp.mobi.R;
import com.itzhp.mobi.user.Login;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HomePage extends AppCompatActivity {
TextInputEditText fdate,todate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        fdate=findViewById(R.id.date1);
        todate=findViewById(R.id.todate);

    }

    public void fromdate(View view) {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mdiDialog = new DatePickerDialog(HomePage.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String mm, dd;
                monthOfYear = monthOfYear + 01;
                if (monthOfYear < 10) {

                    mm = "0" + monthOfYear;
                } else {
                    mm = String.valueOf(monthOfYear);
                }
                if (dayOfMonth < 10) {

                    dd = "0" + dayOfMonth;
                } else {
                    dd = String.valueOf(dayOfMonth);
                }

                fdate.setText(year + "-" + mm + "-" + dd);

            }
        }, mYear, mMonth, mDay);
        mdiDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        mdiDialog.show();
    }

    public void todate(View view) {
        if(fdate.getText().toString().isEmpty()) {
            fdate.setError("Please Select From Date");
        }else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date fd = null;
            long lfd = 0;
            try {
                fd = sdf.parse(fdate.getText().toString());
                lfd = fd.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }


            Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog mdiDialog = new DatePickerDialog(HomePage.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    String mm, dd;
                    monthOfYear = monthOfYear + 01;
                    if (monthOfYear < 10) {

                        mm = "0" + monthOfYear;
                    } else {
                        mm = String.valueOf(monthOfYear);
                    }
                    if (dayOfMonth < 10) {

                        dd = "0" + dayOfMonth;
                    } else {
                        dd = String.valueOf(dayOfMonth);
                    }

                    todate.setText(year + "-" + mm + "-" + dd);

                }
            }, mYear, mMonth, mDay);
            mdiDialog.getDatePicker().setMinDate(lfd);
            mdiDialog.show();
        }
    }

    public void book(View view) {
        if(todate.getText().toString().isEmpty()) {
            todate.setError("Please Select From Date");
        }else {
            Intent i = new Intent(HomePage.this, Rooms.class);
            i.putExtra("fdate", fdate.getText().toString());
            i.putExtra("todate", todate.getText().toString());
            startActivity(i);
        }
    }

    public void cout(View view) {
        Intent i = new Intent(HomePage.this, Checkout.class);
        startActivity(i);
    }

    public void Logout(View view) {
        SharedPreferences spln = getSharedPreferences(Config.LOGGEDIN_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor edln = spln.edit();
        edln.putString(Config.LOGIN_SUCCESS, "false");
        edln.apply();
        Intent intent = new Intent(getApplicationContext(), Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}